#include "dbconnection.h"
#include <QSqlQuery>
#include <QSqlRecord>
#include <QVariant>
#include <QStringList>

DBConnection::DBConnection(QObject *parent)
	: QObject(parent)
{

	db = new QSqlDatabase();
	*db = QSqlDatabase::addDatabase("QODBC3");
	db->setDatabaseName("leapsv");
	db->setHostName(host);
	db->setUserName(user);
	db->setPassword(passowrd);

}

void DBConnection::openDB()
{
	if (!db->isOpen())
		db->open();
}

void DBConnection::closeDB()
{
	if (db->isOpen())
		db->close();
}

DBConnection::~DBConnection()
{

}

QStringList DBConnection::getUsers()
{
	QStringList name;
	openDB();
	QSqlQuery query(sqlGetUser, *db);
	int fieldNo = query.record().indexOf("Name");
	while (query.next()) {
		QString name1 = query.value(fieldNo).toString();
		name.append(name1);

		
	}
	db->close();
	return name;
}

QStringList DBConnection::getFunktionslauf(int userid, int funktionID)
{
	QStringList name;
	openDB();
	QSqlQuery query(*db);
	query.prepare(sqlGetLaufFunktion);
	query.bindValue(0, userid);
	query.bindValue(1, funktionID);
	query.exec();
	int fieldNo = query.record().indexOf("Name");
	while (query.next()) {
		QString name1 = query.value(fieldNo).toString();
		name.append(name1);


	}
	db->close();
	return name;
}


QList<AufnahmeItem *> DBConnection::loadFrames(int funktionslauf)
{
	QList<AufnahmeItem *> items;
	QSqlQuery query(*db);
	query.prepare(sqlLoadFrame);
	query.bindValue(0, funktionslauf);
	query.exec();
	int fieldDate = query.record().indexOf("Date");
	int fieldData = query.record().indexOf("Data");
	while (query.next()) {
		AufnahmeItem * item = new AufnahmeItem();
		item->date = query.value(fieldDate).toDateTime();
		QByteArray data1 = query.value(fieldData).toByteArray();
		item->data = data1;
		items.append(item);
		//QString name1 = query.value(fieldNo).toString();
		//name.append(name1);


	}
	
	return items;


}

void DBConnection::addUser(QString name, QString bildung, int alter, bool freiconfig)
{
	
	openDB();
	QSqlQuery query(*db);
	query.prepare(sqlAddUser);
	query.bindValue(0, name);
	query.bindValue(1, alter);
	query.bindValue(2, bildung);
	query.bindValue(3, freiconfig);
	query.exec();
	
	db->close();
}


QStringList DBConnection::getFunktions()
{
	QStringList name;
	openDB();
	QSqlQuery query(sqlGetFunktion, *db);
	int fieldNo = query.record().indexOf("Name");
	while (query.next()) {
		QString name1 = query.value(fieldNo).toString();
		name.append(name1);


	}
	db->close();
	return name;
}

void DBConnection::addFunktion(QString funktion)
{

	openDB();
	QSqlQuery query(*db);
	query.prepare(sqlAddFunktion);
	query.bindValue(0, funktion);

	query.exec();

	db->close();
}




int DBConnection::addFunktionLauf(int funktion, int lauf)
{
	int id = -1;
	QSqlQuery query(*db);
	query.prepare(sqlAddFunktionLauf);
	query.bindValue(0, lauf);
	query.bindValue(1, funktion);
	query.exec();

	id = query.lastInsertId().toInt();

	return id;


}

int DBConnection::addLauf(QString Beschreibung, int userid)
{
	int id = -1;
	QSqlQuery query(*db);
	query.prepare(sqlAddLauf);
	query.bindValue(0, Beschreibung);
	query.bindValue(1, userid);
	query.exec();
	//if (query.next()) {
	//	id = query.value(0).toInt();
	//}
	id = query.lastInsertId().toInt();

	return id;


}

void DBConnection::addFrame(int funktionslauf, QDateTime date, QByteArray data)
{

	
	QSqlQuery query(*db);
	query.prepare(sqlAddFrame);
	query.bindValue(0, funktionslauf);
	query.bindValue(1, date.toString("yyyy-MM-dd HH:mm:ss.zzz"));
	query.bindValue(2, data);

	query.exec();


}