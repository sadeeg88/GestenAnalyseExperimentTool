#ifndef DBCONNECTION_H
#define DBCONNECTION_H

#include <QObject>
#include <QSqlDatabase>
#include <QDateTime>
#include "aufnahmemanager.h"

class DBConnection : public QObject
{
	Q_OBJECT

public:
	DBConnection(QObject *parent);
	~DBConnection();
	QStringList getUsers();
	void addUser(QString name, QString bildung, int alter, bool freiconfig);
	void addFunktion(QString funktion);
	QStringList getFunktions();
	int addLauf(QString Beschreibung, int userid);
	void openDB();
	void closeDB();
	int addFunktionLauf(int funktion, int lauf);
	void addFrame(int funktionslauf, QDateTime date, QByteArray data);
	QList<AufnahmeItem *> loadFrames(int funktionslauf);
	QStringList getFunktionslauf(int userid, int funktionID);


private:

	QSqlDatabase * db;



	const QString host = "localhost";
	const QString dbNAME = "leapexp";
	const QString user = "sascha";
	const QString passowrd = "dragon#19";


	const QString sqlGetUser = "Select CONCAT(CAST(IdUser AS CHAR(5)),' ' , Name) AS Name from user order by IdUser ASC";
	const QString sqlAddUser = "INSERT INTO user (Name,cAlter,Bildung,FreiConf) VALUES (?,?,?,?)";
	
	const QString sqlGetFunktion = "Select CONCAT(CAST(FunktionId AS CHAR(5)),' ' , Name) AS Name from funktion order by FunktionId ASC";
	const QString sqlAddFunktion = "INSERT INTO funktion (Name) VALUES (?)";
	
	const QString sqlAddFrame = "INSERT INTO frame (FunktionLauf_idFunktionLauf,Date,Data) VALUES (?,?,?);";
	const QString sqlLoadFrame = "SELECT Date,Data FROM frame WHERE FunktionLauf_idFunktionLauf = ? ORDER BY Date ASC;";
	
	const QString sqlAddLauf = "INSERT INTO Lauf (Beschreibung,User_idUser) VALUES(?,?);";

	const QString sqlAddFunktionLauf = "INSERT INTO FunktionLauf (Lauf_idLauf, Funktion_FunktionId) VALUES (?,?);";

	const QString sqlInsertID = "Select LAST_INSERT_ID();";

	const QString sqlGetLaufFunktion = "SELECT CONCAT(CAST(fl.idFunktionLauf AS CHAR(5)),' ', l.Beschreibung) AS Name FROM lauf l INNER JOIN funktionlauf fl ON l.idLauf = fl.Lauf_idLauf where l.User_IdUser = ? AND fl.Funktion_FunktionId = ?;";
};

#endif // DBCONNECTION_H
