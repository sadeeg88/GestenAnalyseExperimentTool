#include "handview.h"
#include "LeapUtil.h"
#include "LeapUtilGL.h"
#include <QtGlobal> 

using namespace LeapUtilGL;

HandView::HandView(QWidget *parent)
	: QGLWidget(QGLFormat(QGL::SampleBuffers), parent)
{
	xRot = 0;
	yRot = 0;
	zRot = 0;

}

HandView::~HandView()
{

}

static void qNormalizeAngle(int &angle)
{
	while (angle < 0)
		angle += 360 * 16;
	while (angle > 360)
		angle -= 360 * 16;
}

void HandView::initializeGL()
{
	// Set up the rendering context, load shaders and other resources, etc.:
	f = QOpenGLContext::currentContext()->functions();
	//f->glClearColor(0.0f, 0.0f, 0.0f, 0.0f);


	glEnable(GL_BLEND);

	glEnable(GL_DEPTH_TEST);
	glDepthMask(true);
	glDepthFunc(GL_LESS);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);

	//
	//	//OpenGLHelpers::clear(Colours::black.withAlpha(0.0f));

	//	

	//	m_camera.SetupGLProjection();

	//	m_camera.SetupGLView();

	//	/// JUCE turns off the depth test every frame when calling paint.
	//	glEnable(GL_DEPTH_TEST);
	//	glDepthMask(true);
	//	glDepthFunc(GL_LESS);

	//	glEnable(GL_BLEND);
	//	glDisable(GL_LIGHTING);


	
	
}

void HandView::resizeGL(int w, int h)
{
	
	
	
	
	setupscene();

	
}

void HandView::setupscene()
{
	//m_camera.SetupGLProjection();
	m_camera.SetAspectRatio(width() / static_cast<float>(height()));

	//m_camera.SetVerticalFOVDegrees(45.0f);
	m_camera.SetClipPlanes(100.0f, 1800.0f);
	m_camera.SetOrbitTarget(Leap::Vector(0,0,-150.0f));
	m_camera.SetPOVLookAt(Leap::Vector(0, 400, 400), m_camera.GetOrbitTarget());

	m_camera.SetupGLProjection();
	m_camera.ResetGLView();






	// left, high, near - corner light
	LeapUtilGL::GLVector4fv vLight0Position(-3.0f, 3.0f, -3.0f, 1.0f);
	// right, near - side light
	LeapUtilGL::GLVector4fv vLight1Position(3.0f, 0.0f, -1.5f, 1.0f);
	// near - head light
	LeapUtilGL::GLVector4fv vLight2Position(0.0f, 0.0f, -3.0f, 1.0f);
	/// JUCE turns off the depth test every frame when calling paint.
	glEnable(GL_DEPTH_TEST);
	glDepthMask(true);
	glDepthFunc(GL_LESS);
	glEnable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);
	const GLfloat light_ambient2[4] = { 0.2f, 0.2f, 2.0, 1.0 };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_ambient2);
	glLightfv(GL_LIGHT0, GL_POSITION, vLight0Position);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, GLColor(0.5f, 0.40f, 0.40f, 1.0f));
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient2);
	glLightfv(GL_LIGHT1, GL_POSITION, vLight1Position);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, GLColor(0.0f, 0.0f, 0.25f, 1.0f));
	glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient2);
	glLightfv(GL_LIGHT2, GL_POSITION, vLight2Position);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, GLColor(0.15f, 0.15f, 0.15f, 1.0f));
	glLightfv(GL_LIGHT2, GL_AMBIENT, light_ambient2);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);

	m_camera.SetupGLView();

}

void HandView::paintGL()
{
	glLoadIdentity();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//



	//glTranslatef(0.0, 0.0, -10.0);
	//glTranslatef(0.0, 0.0, -5.0);
	setupscene();
	glRotatef(xRot / 16.0, 1.0, 0.0, 0.0);
	glRotatef(yRot / 16.0, 0.0, 1.0, 0.0);
	glRotatef(zRot / 16.0, 0.0, 0.0, 1.0);
	//draw();

	{
		LeapUtilGL::GLMatrixScope gridMatrixScope;
		glTranslatef(0, 150.0f, -150.0f);
		glScalef(300, 300, 300);
		LeapUtilGL::drawGrid(LeapUtilGL::kPlane_XY, 20, 20);
	}
	{
		LeapUtilGL::GLMatrixScope gridMatrixScope;
		glTranslatef(0, 0.0f, 0.0f);
		glScalef(300, 300, 300);
		LeapUtilGL::drawGrid(LeapUtilGL::kPlane_ZX, 20, 20);
		//LeapUtilGL::drawAxes();
	}
	if (currentFrame.isValid())
	{
		draw();
	}

	//m_camera.ResetGLView();


	glEnd();
	makeCurrent();
}


void HandView::draw()
{
	qDebug() << "Draw";
	//if (currentFrame.isValid())
	//{
	glPushMatrix();

	//drawPointables(currentFrame);
	LeapUtilGL::GLMatrixScope gridMatrixScope;
	const float fScale = 0.5f;


	qDebug() << currentFrame.hands().count();
		Leap::HandList hands = currentFrame.hands();
		for (Leap::HandList::const_iterator hl = hands.begin(); hl != hands.end(); ++hl) {
			const Leap::Hand hand = *hl;
			//glTranslatef(0, -6.5f, -6.5f);
			//glScalef(0.1f, 0.1f, 0.1f);
			Leap::Vector v1 = hand.palmPosition()* 1;
			//glTranslatef(v1.x,v1.y,v1.z);
			LeapUtilGL::drawSkeletonHand(hand, LeapUtilGL::GLVector4fv(0.0f, 0.0f, 1.0f, 1.0f),LeapUtilGL::GLVector4fv(0.0f, 1.0f, 0.0f, 1.0f));
			//drawSphere(kStyle_Solid);
			qDebug() << "DrawHand";
		
	//	}
	}
		glScalef(fScale, fScale, fScale);
}

void HandView::drawPointables(Leap::Frame frame)
{
	LeapUtilGL::GLAttribScope colorScope(GL_CURRENT_BIT | GL_LINE_BIT);

	const Leap::PointableList& pointables = frame.pointables();

	const float fScale =  0.05f;

	glLineWidth(3.0f);

	for (size_t i = 0, e = pointables.count(); i < e; i++)
	{
		const Leap::Pointable&  pointable = pointables[i];
		Leap::Vector            vStartPos = m_mtxFrameTransform.transformPoint(pointable.tipPosition() * 0.0075f);
		Leap::Vector            vEndPos = m_mtxFrameTransform.transformDirection(pointable.direction()) * -0.25f;
		//const uint32_t          colorIndex = static_cast<uint32_t>(pointable.id()) % kNumColors;

		glColor3f(1, 0, 0);

		{
			LeapUtilGL::GLMatrixScope matrixScope;

			glTranslatef(vStartPos.x, vStartPos.y, vStartPos.z);

			glBegin(GL_LINES);

			glVertex3f(0, 0, 0);
			glVertex3fv(vEndPos.toFloatPointer());

			glEnd();

			glScalef(fScale, fScale, fScale);

			LeapUtilGL::drawSphere(LeapUtilGL::kStyle_Solid);
		}
	}
}

void HandView::setXRotation(int angle)
{
	qNormalizeAngle(angle);
	if (angle != xRot) {
		xRot = angle;
		emit xRotationChanged(angle);
		updateGL();
	}
}

void HandView::setYRotation(int angle)
{
	qNormalizeAngle(angle);
	if (angle != yRot) {
		yRot = angle;
		emit yRotationChanged(angle);
		updateGL();
	}
}

void HandView::setZRotation(int angle)
{
	qNormalizeAngle(angle);
	if (angle != zRot) {
		zRot = angle;
		emit zRotationChanged(angle);
		updateGL();
	}
}


void HandView::mouseMoveEvent(QMouseEvent *event)
{
	int dx = event->x() - lastPos.x();
	int dy = event->y() - lastPos.y();

	if (event->buttons() & Qt::LeftButton) {
		setXRotation(xRot + 8 * dy);
		setYRotation(yRot + 8 * dx);
	}
	else if (event->buttons() & Qt::RightButton) {
		setXRotation(xRot + 8 * dy);
		setZRotation(zRot + 8 * dx);
	}


	lastPos = event->pos();
}

void HandView::mousePressEvent(QMouseEvent *event)
{
	lastPos = event->pos();
}

void HandView::wheelEvent(QWheelEvent *event)
{
	//m_camera.OnMouseWheel(event->delta());
	//updateGL();
}

void HandView::updateFrame()
{
	updateGL();
}
//void HandView::drawGrid(ePlane plane, unsigned int horizSubdivs, unsigned int vertSubdivs)
//{
////	const float fHalfGridSize = 0.5f;
////	const float fHGridStep = (fHalfGridSize + fHalfGridSize) / static_cast<float>(Max (horizSubdivs, 1u));
////	const float fVGridStep = (fHalfGridSize + fHalfGridSize) / static_cast<float>(Max(vertSubdivs, 1u));
////	const float fHEndStep = fHalfGridSize + fHGridStep;
////	const float fVEndStep = fHalfGridSize + fVGridStep;
////
//////	GLAttribScope lightingScope(GL_LIGHTING_BIT);
////
////	f->glDisable(GL_LIGHTING);
////
////	glBegin(GL_LINES);
////
////	switch (plane)
////	{
////	case kPlane_XY:
////		for (float x = -fHalfGridSize; x < fHEndStep; x += fHGridStep)
////		{
////			glVertex3f(x, -fHalfGridSize, 0);
////			glVertex3f(x, fHalfGridSize, 0);
////		}
////
////		for (float y = -fHalfGridSize; y < fVEndStep; y += fVGridStep)
////		{
////			glVertex3f(-fHalfGridSize, y, 0);
////			glVertex3f(fHalfGridSize, y, 0);
////		}
////		break;
////
////	case kPlane_YZ:
////		for (float y = -fHalfGridSize; y < fHEndStep; y += fHGridStep)
////		{
////			glVertex3f(0, y, -fHalfGridSize);
////			glVertex3f(0, y, fHalfGridSize);
////		}
////
////		for (float z = -fHalfGridSize; z < fVEndStep; z += fVGridStep)
////		{
////			glVertex3f(0, -fHalfGridSize, z);
////			glVertex3f(0, fHalfGridSize, z);
////		}
////		break;
////
////	case kPlane_ZX:
////		for (float z = -fHalfGridSize; z < fHEndStep; z += fHGridStep)
////		{
////			glVertex3f(-fHalfGridSize, 0, z);
////			glVertex3f(fHalfGridSize, 0, z);
////		}
////
////		for (float x = -fHalfGridSize; x < fVEndStep; x += fVGridStep)
////		{
////			glVertex3f(x, 0, -fHalfGridSize);
////			glVertex3f(x, 0, fHalfGridSize);
////		}
////		break;
////	}
////
////	glEnd();
//}

