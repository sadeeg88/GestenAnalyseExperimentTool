#ifndef GESTENANALYSEEXPERIMENTTOOL_H
#define GESTENANALYSEEXPERIMENTTOOL_H

#include <QtWidgets/QMainWindow>
#include "ui_gestenanalyseexperimenttool.h"

class GestenAnalyseExperimentTool : public QMainWindow
{
	Q_OBJECT

public:
	GestenAnalyseExperimentTool(QWidget *parent = 0);
	~GestenAnalyseExperimentTool();

private:
	Ui::GestenAnalyseExperimentToolClass ui;
	void refreshView();
	void refreshFunkLaufView();

	int ProbantenId = -1;
	int FunktionsID = -1;


	public slots:
	void starteAufnahme();
	void starteProbantenView();
	void starteFunktionView();
	void changeSelction();
	void startPlay(QListWidgetItem * item);


};

#endif // GESTENANALYSEEXPERIMENTTOOL_H
