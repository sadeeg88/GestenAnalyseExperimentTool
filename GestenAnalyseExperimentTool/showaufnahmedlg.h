#ifndef SHOWAUFNAHMEDLG_H
#define SHOWAUFNAHMEDLG_H

#include <QDialog>
#include "ui_showaufnahmedlg.h"
#include "handview.h"
#include "aufnahmemanager.h"

class ShowAufnahmeDlg : public QDialog
{
	Q_OBJECT

public:
	ShowAufnahmeDlg(int laufFunktionId ,QWidget *parent = 0);
	~ShowAufnahmeDlg();

private:
	Ui::ShowAufnahmeDlg ui;
	AufnahmeManager * manager = NULL;
	HandView * m_pHandView = NULL;
	int m_laufFunktionId = 0;
	public slots:
	void refresh();

};

#endif // SHOWAUFNAHMEDLG_H
