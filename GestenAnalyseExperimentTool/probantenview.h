#ifndef PROBANTENVIEW_H
#define PROBANTENVIEW_H

#include <QDialog>
#include "ui_probantenview.h"

class ProbantenView : public QDialog
{
	Q_OBJECT

public:
	ProbantenView(QWidget *parent = 0);
	~ProbantenView();

private:
	Ui::Probanten ui;
public slots:
	void addUser();

};

#endif // PROBANTENVIEW_H
