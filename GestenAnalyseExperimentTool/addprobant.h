#ifndef ADDPROBANT_H
#define ADDPROBANT_H

#include <QDialog>
#include "ui_addprobant.h"

class AddProbant : public QDialog
{
	Q_OBJECT

public:
	AddProbant(QWidget *parent = 0);
	~AddProbant();

private:
	Ui::AddProbant ui;

	public slots:
	void save();
};

#endif // ADDPROBANT_H
