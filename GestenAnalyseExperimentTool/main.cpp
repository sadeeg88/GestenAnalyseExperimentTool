#include "gestenanalyseexperimenttool.h"
#include "aufnahmedlg.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	GestenAnalyseExperimentTool w;
	w.show();

	return a.exec();
}
