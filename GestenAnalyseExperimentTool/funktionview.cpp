#include "funktionview.h"
#include "dbconnection.h"
#include <QInputDialog>

FunktionView::FunktionView(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	DBConnection con(this);
	ui.listWidget->addItems(con.getFunktions());
	connect(ui.btnAdd, SIGNAL(clicked()), this, SLOT(addFunktion()));
	this->setWindowTitle("Funktionen");
}

FunktionView::~FunktionView()
{

}

void FunktionView::addFunktion()
{
	DBConnection con(this);
	bool ok;
	QString text = QInputDialog::getText(this, tr("QInputDialog::getText()"),
		tr("Funktionname:"), QLineEdit::Normal,
		"", &ok);
	if (ok && !text.isEmpty())
	{
		con.addFunktion(text);
	}

	ui.listWidget->clear();
	ui.listWidget->addItems(con.getFunktions());
}