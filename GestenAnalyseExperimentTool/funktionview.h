#ifndef FUNKTIONVIEW_H
#define FUNKTIONVIEW_H

#include <QObject>
#include <QDialog>
#include "ui_probantenview.h"

class FunktionView : public QDialog
{
	Q_OBJECT

public:
	FunktionView(QWidget *parent = 0);
	~FunktionView();

private:
	Ui::Probanten ui;
	public slots:
	void addFunktion();

};

#endif // FUNKTIONVIEW_H
