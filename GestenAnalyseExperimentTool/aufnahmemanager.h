#ifndef AUFNAHMEMANAGER_H
#define AUFNAHMEMANAGER_H

#include <QObject>
#include <QQueue>
#include <QString>
#include <QDateTime>
#include "handview.h"
#include "Leap.h"

class AufnahmeItem
{
public:
	QDateTime date;
	QByteArray data;
	QString picData;
};


class AufnahmeManager : public QObject
{
	Q_OBJECT

public:
	AufnahmeManager(QObject *parent);
	~AufnahmeManager();
	void setAktiv(bool aktiv);
	void addData(AufnahmeItem * item);
	void clear();
	void save(int funktionslauf);
	int pos = 0;
	QList<Leap::Frame> m_frames;
	QList<qint64> time;
	int count(){
		return m_queue.count();
	}
	bool isAktiv()
	{
		return m_aktiv;
	}
	void load(int funktionslauf);
	void calcPlayTime();
	void play();
	HandView * m_pHandview = NULL;
		
public slots:
     void nextFrame();


private:
	QQueue<AufnahmeItem *> m_queue;
	QList<AufnahmeItem *> m_playback;

	bool m_aktiv = false;
	QTimer * m_pTimer;
	float speed = 1.0f;
	
signals:
	void newFrame();
	
};

#endif // AUFNAHMEMANAGER_H
