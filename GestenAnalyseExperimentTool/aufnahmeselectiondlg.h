#ifndef AUFNAHMESELECTIONDLG_H
#define AUFNAHMESELECTIONDLG_H

#include <QDialog>
#include "ui_aufnahmeselectiondlg.h"

class AufnahmeSelectionDlg : public QDialog
{
	Q_OBJECT

public:
	AufnahmeSelectionDlg(QWidget *parent = 0);
	~AufnahmeSelectionDlg();

public slots:
	void next();

private:
	Ui::AufnahmeSelectionDlg ui;
};

#endif // AUFNAHMESELECTIONDLG_H
