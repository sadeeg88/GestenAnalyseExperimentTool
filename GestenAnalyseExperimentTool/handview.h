#ifndef HANDVIEW_H
#define HANDVIEW_H

#include <QGLWidget>
#include <QOpenGLFunctions>


#if !defined(_HANDVIEW_H)
#if defined(WIN32)
#include <windows.h>
#include <GL/gl.h>
#elif defined(__MACH__)
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif
#endif // !__GL_H__

#include "LeapUtil.h"
#include "LeapUtilGL.h"
#include "Leap.h"

#include <QMatrix4x4>
#include <QMouseEvent>
#include <GL/glu.h>

struct GLColor
{
	GLColor() : r(1), g(1), b(1), a(1) {}
	GLColor(float _r, float _g, float _b, float _a = 1)
		: r(_r), g(_g), b(_b), a(_a)
	{}
	operator const GLfloat*() const { return &r; }
	GLfloat r, g, b, a;
};

class HandView : public QGLWidget
{
	Q_OBJECT

public slots:
	void updateFrame();

public:
	HandView(QWidget *parent);
	~HandView();
	Leap::Frame currentFrame;
	
private:
	QOpenGLFunctions *f = NULL;
private:
	//LeapUtilGL::CameraGL * camera = NULL;
	LeapUtilGL::CameraGL m_camera;


protected:
	void initializeGL();
	void resizeGL(int w, int h);
	void paintGL();
	void draw();
	void setupscene();
	void drawPointables(Leap::Frame frame);
	Leap::Matrix m_mtxFrameTransform;
	//void drawGrid(ePlane plane, unsigned int horizSubdivs, unsigned int vertSubdivs);

	public slots:
	// slots for xyz-rotation slider
	void setXRotation(int angle);
	void setYRotation(int angle);
	void setZRotation(int angle);

signals:
	// signaling rotation from mouse movement
	void xRotationChanged(int angle);
	void yRotationChanged(int angle);
	void zRotationChanged(int angle);

private:

	int xRot;
	int yRot;
	int zRot;

	

	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void wheelEvent(QWheelEvent *event);

	QPoint lastPos;



	
};

#endif // HANDVIEW_H
