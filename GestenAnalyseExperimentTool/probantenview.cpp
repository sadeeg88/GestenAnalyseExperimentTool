#include "probantenview.h"
#include "dbconnection.h"
#include "addprobant.h"

ProbantenView::ProbantenView(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	DBConnection con(this);
	ui.listWidget->addItems(con.getUsers());
	connect(ui.btnAdd, SIGNAL(clicked()), this, SLOT(addUser()));
}

ProbantenView::~ProbantenView()
{

}

void ProbantenView::addUser()
{
	AddProbant addpro(this);
	addpro.show();
	addpro.exec();

	DBConnection con(this);
	ui.listWidget->clear();
	ui.listWidget->addItems(con.getUsers());
}