#include "aufnahmedlg.h"
#include "dbconnection.h"
#include "tools.h"

AufnahmeDlg::AufnahmeDlg(QWidget *parent, int lauf)
	: QDialog(parent)
{
	m_lauf = lauf;
	setupUi(this);
	m_pHandview = new HandView(this);
	m_pAufnahmeManager = new AufnahmeManager(this);

	m_pHandview->setGeometry(QRect(290, 10, 981, 581));
	listenermy.pView = m_pHandview;
	listenermy.mananger = m_pAufnahmeManager;
	controller.setPolicy(Leap::Controller::PolicyFlag::POLICY_IMAGES);
	controller.addListener(listenermy);
	connect(&listenermy, SIGNAL(newFrame()), m_pHandview, SLOT(updateFrame()));
	connect(&listenermy, SIGNAL(newFrame()), this, SLOT(newFrame()));
	
	DBConnection con(this);
	con.openDB();
	m_Funktions.append(con.getFunktions());
	con.closeDB();

	if (m_Funktions.count() > 0)
		nextFunktion();
	connect(btnStartStop, SIGNAL(clicked()), this, SLOT(start()));
	connect(btnWiederUeber, SIGNAL(clicked()), this, SLOT(uberspringen()));

}

AufnahmeDlg::~AufnahmeDlg()
{
	controller.removeListener(listenermy);
}

void AufnahmeDlg::weiter()
{
	disconnect(btnWiederUeber, SIGNAL(clicked()), this, SLOT(wiederholen()));
	disconnect(btnWeiter, SIGNAL(clicked()), this, SLOT(weiter()));
	DBConnection con(this);
	con.openDB();
	int funktionlauf = con.addFunktionLauf(Tools::getID(m_Funktions.at(m_nextFunktionPos - 1)), m_lauf);
	con.closeDB();

	m_pAufnahmeManager->save(funktionlauf);

	if (isEnde())
	{
		ende();
	}
	else
	{
		nextFunktion();
		connect(btnStartStop, SIGNAL(clicked()), this, SLOT(start()));
		btnWeiter->setEnabled(false);
		connect(btnWiederUeber, SIGNAL(clicked()), this, SLOT(uberspringen()));
		btnStartStop->setText("Start");
		btnStartStop->setEnabled(true);
		btnWeiter->setEnabled(false);
	}

}

void AufnahmeDlg::start()
{
	startTime = QDateTime::currentDateTime();
	disconnect(btnStartStop, SIGNAL(clicked()), this, SLOT(start()));
	disconnect(btnWiederUeber, SIGNAL(clicked()), this, SLOT(uberspringen()));
	
	btnStartStop->setText("Stop");
	btnWiederUeber->setEnabled(false);
	connect(btnStartStop, SIGNAL(clicked()), this, SLOT(stop()));
	m_pAufnahmeManager->setAktiv(true);
}

void AufnahmeDlg::stop()
{
	btnWiederUeber->setText("Wiederholen");
	btnWiederUeber->setEnabled(true);
	disconnect(btnStartStop, SIGNAL(clicked()), this, SLOT(stop()));
	btnStartStop->setText("Start");
	btnStartStop->setEnabled(false);
	btnWeiter->setEnabled(true);

	connect(btnWiederUeber, SIGNAL(clicked()), this, SLOT(wiederholen()));
	connect(btnWeiter, SIGNAL(clicked()), this, SLOT(weiter()));
	m_pAufnahmeManager->setAktiv(false);
}

void AufnahmeDlg::wiederholen()
{
	startTime = QDateTime::currentDateTime();
	disconnect(btnWiederUeber, SIGNAL(clicked()), this, SLOT(wiederholen()));
	disconnect(btnWeiter, SIGNAL(clicked()), this, SLOT(weiter()));
		btnStartStop->setEnabled(true);
	btnWiederUeber->setText(tr("‹berspringen"));
	btnStartStop->setText("Start");
	btnWeiter->setEnabled(false);
	connect(btnStartStop, SIGNAL(clicked()), this, SLOT(start()));
	connect(btnWiederUeber, SIGNAL(clicked()), this, SLOT(uberspringen()));
	m_pAufnahmeManager->clear();

}

void AufnahmeDlg::uberspringen()
{
	disconnect(btnStartStop, SIGNAL(clicked()), this, SLOT(start()));
	disconnect(btnWiederUeber, SIGNAL(clicked()), this, SLOT(uberspringen()));

	if (isEnde())
	{
		ende();
	}
	else
	{
		nextFunktion();
		connect(btnStartStop, SIGNAL(clicked()), this, SLOT(start()));
		connect(btnWiederUeber, SIGNAL(clicked()), this, SLOT(uberspringen()));
		btnWeiter->setEnabled(false);
	}
}

void  AufnahmeDlg::nextFunktion()
{
	//Speichern

	//Clear
	m_pAufnahmeManager->clear();
	this->setWindowTitle(m_Funktions.at(m_nextFunktionPos));
	funktionsname->setText( m_Funktions.at(m_nextFunktionPos));
	btnWeiter->setEnabled(false);
	btnWiederUeber->setText(tr("‹berspringen"));
	btnStartStop->setText("Start");
	m_nextFunktionPos++;
}

void AufnahmeDlg::ende()
{
	funktionsname->setText("Ende :) Geschafft!");
	btnStartStop->setEnabled(false);
	btnWiederUeber->setEnabled(false);
	btnWeiter->setEnabled(true);
	btnWeiter->setText("Beenden");
	connect(btnWiederUeber, SIGNAL(clicked()), this, SLOT(done(0)));
}


bool AufnahmeDlg::isEnde()
{
	if (m_nextFunktionPos > (m_Funktions.count()-1))
		return true;
	else
		return false;

}

void  AufnahmeDlg::newFrame()
{
	lbFrames->setText(QString::number(m_pAufnahmeManager->count(), 10));

	if (m_pAufnahmeManager->isAktiv())
	{
		stopTime = QDateTime::currentDateTime();
		qint64 sec = startTime.secsTo(stopTime);
		qint64 min = sec / 60;
		sec = sec - (min * 60);
		QString ssec = QString::number(sec);
		if (sec < 9)
			ssec = "0" + ssec;
		QString smin = QString::number(min);


		lbTime->setText(smin + ":"+ssec);
	}
}