#include "showaufnahmedlg.h"

ShowAufnahmeDlg::ShowAufnahmeDlg(int laufFunktionId, QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	manager = new AufnahmeManager(this);
	m_pHandView = new HandView(this);
	m_pHandView->setGeometry(QRect(180, 0, 1101, 600));
	m_laufFunktionId = laufFunktionId;
	manager->load(laufFunktionId);
	manager->m_pHandview = m_pHandView;
	manager->calcPlayTime();
	manager->play();
	connect(manager, SIGNAL(newFrame()), m_pHandView, SLOT(updateFrame()));
	connect(manager, SIGNAL(newFrame()), this, SLOT(refresh()));
}

ShowAufnahmeDlg::~ShowAufnahmeDlg()
{

}
void ShowAufnahmeDlg::refresh()
{
	ui.FRAME->setText(QString::number(manager->pos) + '/' + QString::number(manager->m_frames.count()));
}