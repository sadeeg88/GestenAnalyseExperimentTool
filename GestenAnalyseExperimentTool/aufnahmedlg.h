#ifndef AUFNAHMEDLG_H
#define AUFNAHMEDLG_H

#include <QDialog>
#include "ui_aufnahmedlg.h"
#include "handview.h"
#include <iostream>
#include <string.h>
#include "Leap.h"
#include "leaplistenner.h"

class AufnahmeDlg : public QDialog, public Ui::AufnahmeDlg
{
	Q_OBJECT

public:
	AufnahmeDlg(QWidget *parent , int lauf);
	~AufnahmeDlg();

	HandView * m_pHandview;
	AufnahmeManager * m_pAufnahmeManager = NULL;

	LeapListenner listenermy;
	QDateTime startTime = QDateTime::currentDateTime();
	QDateTime stopTime = QDateTime::currentDateTime();
	Controller controller;
private:
	int m_lauf = 0;
	int m_nextFunktionPos = 0;
	QStringList m_Funktions;
	void nextFunktion();
	bool isEnde();
	void ende();

public slots:
void weiter();
void start();
void stop();
void wiederholen();
void uberspringen();
void newFrame();


};

#endif // AUFNAHMEDLG_H
