#include "aufnahmeselectiondlg.h"
#include "dbconnection.h"
#include "tools.h"
#include "aufnahmedlg.h"

AufnahmeSelectionDlg::AufnahmeSelectionDlg(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	DBConnection con(this);
	con.openDB();
	ui.comboBox->addItems(con.getUsers());
	con.closeDB();
	connect(ui.btnWeiter, SIGNAL(clicked()), this, SLOT(next()));
}

AufnahmeSelectionDlg::~AufnahmeSelectionDlg()
{

}

void AufnahmeSelectionDlg::next()
{
	int userid = Tools::getID(ui.comboBox->currentText());
	
	DBConnection con(this);
	con.openDB();
	int laufid = con.addLauf(ui.lnBeschreibung->text(), userid);
	con.closeDB();
	AufnahmeDlg dlg(this, laufid);
	dlg.show();
	dlg.exec();	
	close();

}
