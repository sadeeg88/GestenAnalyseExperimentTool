#include "gestenanalyseexperimenttool.h"
#include "aufnahmedlg.h"
#include "probantenview.h"
#include "funktionview.h"
#include "aufnahmeselectiondlg.h"
#include "dbconnection.h"
#include "tools.h"
#include "showaufnahmedlg.h"

GestenAnalyseExperimentTool::GestenAnalyseExperimentTool(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	connect(ui.actionStarte_aufnahme, SIGNAL(triggered(bool)), this, SLOT(starteAufnahme()));
	connect(ui.actionProbanten, SIGNAL(triggered(bool)), this, SLOT(starteProbantenView()));
	connect(ui.actionFunktionen, SIGNAL(triggered(bool)), this, SLOT(starteFunktionView()));
	connect(ui.listProbanten, SIGNAL(itemSelectionChanged()), this, SLOT(changeSelction()));
	connect(ui.listFunktionen, SIGNAL(itemSelectionChanged()), this, SLOT(changeSelction()));
	connect(ui.listLauf, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(startPlay(QListWidgetItem *)));
	refreshView();

	

}

void GestenAnalyseExperimentTool::startPlay(QListWidgetItem * item)
{
	if (item != NULL)
	{
		ShowAufnahmeDlg * p_Show = new ShowAufnahmeDlg(Tools::getID(item->text()));
		p_Show->show();
		p_Show->exec();

	}
}

void GestenAnalyseExperimentTool::changeSelction()
{
	QListWidgetItem * item = ui.listProbanten->currentItem();
	if (item != NULL)
	{
		ProbantenId = Tools::getID(item->text());
	}
	else
		ProbantenId = -1;

	QListWidgetItem * item2 = ui.listFunktionen->currentItem();
	if (item2 != NULL)
	{
		FunktionsID = Tools::getID(item2->text());
	}
	else
		FunktionsID = -1;

	refreshFunkLaufView();
}

void GestenAnalyseExperimentTool::refreshFunkLaufView()
{
	if ( ProbantenId != -1
		&& FunktionsID != -1)
	{
		DBConnection con(this);
		con.openDB();
		ui.listLauf->clear();
		ui.listLauf->addItems(con.getFunktionslauf(ProbantenId,FunktionsID));
		con.closeDB();
	}
	else
	{
		ui.listLauf->clear();

	}


}



void GestenAnalyseExperimentTool::refreshView()
{
	DBConnection con(this);
	con.openDB();
	ui.listProbanten->clear();
	ui.listProbanten->addItems(con.getUsers());

	ui.listFunktionen->clear();
	ui.listFunktionen->addItems(con.getFunktions());
	con.closeDB();


}

GestenAnalyseExperimentTool::~GestenAnalyseExperimentTool()
{

}

void GestenAnalyseExperimentTool::starteAufnahme()
{
	AufnahmeSelectionDlg * aufnahme = new AufnahmeSelectionDlg(this);
	aufnahme->show();
	aufnahme->exec();
	//refreshView();
	refreshFunkLaufView();
}

void GestenAnalyseExperimentTool::starteProbantenView()
{
	ProbantenView * aufnahme = new ProbantenView(this);
	aufnahme->show();
	aufnahme->exec();
	refreshView();
}

void GestenAnalyseExperimentTool::starteFunktionView()
{
	FunktionView * aufnahme = new FunktionView(this);
	aufnahme->show();
	aufnahme->exec();
	refreshView();
}