#include "aufnahmemanager.h"
#include "dbconnection.h"
#include <QTimer>

AufnahmeManager::AufnahmeManager(QObject *parent)
	: QObject(parent)
{

}

AufnahmeManager::~AufnahmeManager()
{

}
void AufnahmeManager::setAktiv(bool aktiv)
{
	m_aktiv = aktiv;
}

void AufnahmeManager::addData(AufnahmeItem * item)
{
	if (m_aktiv)
	{
		m_queue.enqueue(item);
	}

}
void AufnahmeManager::load(int funktionslauf)
{
	DBConnection con(this);
	con.openDB();
	m_playback.append(con.loadFrames(funktionslauf));
	con.closeDB();
}
void AufnahmeManager::calcPlayTime()
{
	Leap::Controller controller; //An instance must exist
	Leap::Frame * reconstructedFrame;
	for (int pos = 0; pos < (m_playback.count() - 1); pos++)
	{
		time.append(m_playback.at(pos)->date.msecsTo(m_playback.at(pos + 1)->date));
	}
	for (int pos = 0; pos < m_playback.count(); pos++)
	{
		AufnahmeItem * item = m_playback.at(pos);
		std::string frameData;
		frameData.resize(item->data.size(), '\0');
		memcpy(&frameData[0], item->data.data(), item->data.size());

		
		reconstructedFrame = new Leap::Frame();
		reconstructedFrame->deserialize(frameData);
		m_frames.append(*reconstructedFrame);
	}

	
}

void AufnahmeManager::play() 
{
	QTimer::singleShot(time.at(pos), this, SLOT(nextFrame()));
	m_pHandview->currentFrame = m_frames.at(pos);
	emit newFrame();


}

void AufnahmeManager::clear()
{
	for each (AufnahmeItem * x in m_queue)
	{
		delete x;
	}
	m_queue.clear();
}
void AufnahmeManager::save(int funktionslauf)
{
	DBConnection con(this);
	con.openDB();
	while (m_queue.count() > 0)
	{
		AufnahmeItem * item = m_queue.dequeue();
		con.addFrame(funktionslauf, item->date, item->data);
	}
	con.closeDB();
}

void  AufnahmeManager::nextFrame()
{
	
	if ((pos + 1) < m_playback.count())
	{
		if (m_pHandview != NULL)
		{
			m_pHandview->currentFrame = m_frames.at(pos);
			pos++;
			if (pos < time.count())
			{
				QTimer::singleShot(time.at(pos), this, SLOT(nextFrame()));
			}
			emit newFrame();

		}
	}

}