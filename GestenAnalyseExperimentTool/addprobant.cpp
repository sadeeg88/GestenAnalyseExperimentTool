#include "addprobant.h"
#include "dbconnection.h"

AddProbant::AddProbant(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	connect(ui.btnAbbrechen, SIGNAL(clicked()), this, SLOT(done(-1)));
	connect(ui.btnSpeichern, SIGNAL(clicked()), this, SLOT(save()));
}

AddProbant::~AddProbant()
{

}

void AddProbant::save()
{
	DBConnection con(this);
	bool ok;
	int alter = ui.edtAlter->text().toInt(&ok);
	if (!ok)
		alter = 0;

	con.addUser(ui.edtName->text(), ui.EdtBildung->text(), alter, ui.cbFrei->isChecked());

	done(0);

}