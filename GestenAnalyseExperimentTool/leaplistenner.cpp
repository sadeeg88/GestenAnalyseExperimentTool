#include "leaplistenner.h"
#include <iostream>
#include <string.h>
#include <QDebug>
#include "Leap.h"

using namespace Leap;

LeapListenner::LeapListenner(QObject *parent)
	: QObject(parent)
{

}

LeapListenner::~LeapListenner()
{

}


const std::string fingerNames[] = { "Thumb", "Index", "Middle", "Ring", "Pinky" };
const std::string boneNames[] = { "Metacarpal", "Proximal", "Middle", "Distal" };
const std::string stateNames[] = { "STATE_INVALID", "STATE_START", "STATE_UPDATE", "STATE_END" };

void LeapListenner::onInit(const Controller& controller) {
}

void LeapListenner::onConnect(const Controller& controller) {
	qDebug()<< "Connected" ;

}

void LeapListenner::onDisconnect(const Controller& controller) {
	// Note: not dispatched when running in a debugger.
	qDebug() << "Disconnected" ;
}

void LeapListenner::onExit(const Controller& controller) {
	qDebug() << "Exited";
}

void LeapListenner::onFrame(const Controller& controller) {
	// Get LeapListenner most recent frame and report some basic information
	const Frame frame = controller.frame();

	qDebug() << "Frame";
	QDateTime currentTime = QDateTime::currentDateTime();
	

	AufnahmeItem * frameData = new AufnahmeItem();

	const std::string t5 = frame.serialize();
	frameData->date = currentTime;
	frameData->data.fill('\0', t5.size());
	memcpy(frameData->data.data(), &t5[0], t5.size());
		
	/*ImageList images = controller.images();
	int x = images.count();
	if (images.count() > 0)
	{
		const unsigned char * data = images[0].data();
		QImage* img = new QImage(images[0].width(), images[0].height(), QImage::Forma);
		
		int size = images[0].bytesPerPixel() *images[0].width()*images[0].height();
		QByteArray datanew((const char *)data, size);
		img->save("f:\\test.bmp");
	}*/

	if (frame.isValid())
	{
		if (pView != NULL)
			pView->currentFrame = frame;
		if (mananger != NULL)
		{
			mananger->addData(frameData);
			

		}
			
		emit newFrame();
	}
	
}

void LeapListenner::onFocusGained(const Controller& controller) {
	qDebug() << "Focus Gained";
}

void LeapListenner::onFocusLost(const Controller& controller) {
	qDebug() << "Focus Lost";
}

void LeapListenner::onDeviceChange(const Controller& controller) {
	qDebug() << "Device Changed";
	const DeviceList devices = controller.devices();
	/*
	for (int i = 0; i < devices.count(); ++i) {
		qDebug() << "id: ";
		qDebug() << devices[i].toString();
		qDebug() << "  isStreaming: " << (devices[i].isStreaming() ? "true" : "false") ;*/
	
}

void LeapListenner::onServiceConnect(const Controller& controller) {
	qDebug() << "Service Connected" ;
}

void LeapListenner::onServiceDisconnect(const Controller& controller) {
	qDebug() << "Service Disconnected" ;
}
/*
int main(int argc, char** argv) {
	// Create a sample listener and controller
	SampleListener listener;
	Controller controller;

	// Have the sample listener receive events from the controller
	controller.addListener(listener);

	if (argc > 1 && strcmp(argv[1], "--bg") == 0)
		controller.setPolicy(Leap::Controller::POLICY_BACKGROUND_FRAMES);

	// Keep this process running until Enter is pressed
	std::cout << "Press Enter to quit..." << std::endl;
	std::cin.get();

	// Remove the sample listener when done
	controller.removeListener(listener);

	return 0;
}*/
