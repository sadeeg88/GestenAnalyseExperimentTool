#ifndef LEAPLISTENNER_H
#define LEAPLISTENNER_H

#include <QObject>
#include <iostream>
#include <string.h>
#include "Leap.h"
#include "handview.h"
#include "aufnahmemanager.h"

using namespace Leap;

class LeapListenner : public QObject, public  Listener
{
	Q_OBJECT

public:
	LeapListenner(QObject *parent = NULL);
	~LeapListenner();
	virtual void onInit(const Controller&);
	virtual void onConnect(const Controller&);
	virtual void onDisconnect(const Controller&);
	virtual void onExit(const Controller&);
	virtual void onFrame(const Controller&);
	virtual void onFocusGained(const Controller&);
	virtual void onFocusLost(const Controller&);
	virtual void onDeviceChange(const Controller&);
	virtual void onServiceConnect(const Controller&);
	virtual void onServiceDisconnect(const Controller&);
	HandView * pView = NULL;
	AufnahmeManager * mananger  = NULL;

private:
signals :
	void newFrame();

	
};

#endif // LEAPLISTENNER_H




